﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public GameObject[] hand = new GameObject[2];   // References to the hand anchors in the camera rig.
    public GameObject cubePrefab;                   // The prefab to spawn when pressing the trigger.
    public GameObject head;                         // Reference to the head in the camera rig.
    public GameObject[] grab = new GameObject[2];   // The object being held by each hand.
    Vector3 lastPosition;                           // Last position of the Go remote.


    // Start is called before the first frame update
    void Start()
    {
        // To make left and right handed Go controllers easier, check at runtime which is active and make both hands refer to the same controller.
        if (OVRPlugin.GetSystemHeadsetType() == OVRPlugin.SystemHeadset.Oculus_Go)
        {
            if (OVRInput.GetDominantHand() == OVRInput.Handedness.LeftHanded)
                hand[1] = hand[0];
            else
                hand[0] = hand[1];
        }
    }

    // Update is called once per frame
    void Update()
    {
        // If we are running on a Go.
        if(OVRPlugin.GetSystemHeadsetType() == OVRPlugin.SystemHeadset.Oculus_Go)
        {
            UpdateHand(0, OVRInput.GetActiveController(), OVRInput.Button.PrimaryIndexTrigger, OVRInput.Button.PrimaryHandTrigger, 3.0f, (hand[0].transform.position - lastPosition)/Time.deltaTime);
            // Use the touchpad to move.
            if (OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
            {
                Vector2 thumb = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
                Vector3 forward = hand[0].transform.forward;
                Vector3 right = hand[0].transform.right;
                forward.y = 0.0f;
                right.y = 0.0f;
                transform.position += (forward.normalized * thumb.y + right.normalized * thumb.x) * Time.deltaTime * 1.5f;
            }
            lastPosition = hand[0].transform.position;
        }
        else
        {
            // Update both left and right hands for Rift or Quest.
            UpdateHand(0, OVRInput.Controller.LTouch, OVRInput.Button.PrimaryIndexTrigger, OVRInput.Button.PrimaryHandTrigger, 0, Vector3.zero);
            UpdateHand(1, OVRInput.Controller.RTouch, OVRInput.Button.PrimaryIndexTrigger, OVRInput.Button.PrimaryHandTrigger, 0, Vector3.zero);
        }
    }

    void UpdateHand(int handNumber, OVRInput.Controller controller, OVRInput.Button indexTrigger, OVRInput.Button handTrigger, float strength, Vector3 velocity)
    {
        // We've got something in our hand.
        if (grab[handNumber])
        {
            Rigidbody rb = grab[handNumber].GetComponent<Rigidbody>();
            // Grabbed things are kinematic, so use these Move functions.
            rb.MovePosition(hand[handNumber].transform.position);
            rb.MoveRotation(hand[handNumber].transform.rotation);
        }
        
        // Shoot a cube from our hands
        if (OVRInput.GetDown(indexTrigger, controller))
        {
            GameObject spawn = Instantiate(cubePrefab, hand[handNumber].transform.position, hand[handNumber].transform.rotation);
            spawn.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(controller) * 1.5f + hand[handNumber].transform.forward * strength + velocity;
        }

        // Try to grab something
        if (OVRInput.GetDown(handTrigger, controller) && !grab[handNumber])
        {
            // Find all colliders within 10cm.
            Collider[] colliders = Physics.OverlapSphere(hand[handNumber].transform.position, 0.1f);
            for (int i = 0; i < colliders.Length; ++i)
            {
                Rigidbody rb = colliders[i].gameObject.GetComponent<Rigidbody>();
                // Only grab objects with a rigib body.
                if (rb)
                {
                    grab[handNumber] = colliders[i].gameObject;
                    rb.isKinematic = true;  // Make the object kinematic.
                    break;  // We only want one object, so break out.
                }
            }
        }

        // Drop the thing we are holding.
        if (OVRInput.GetUp(handTrigger, controller) && grab[handNumber])
        {
            Rigidbody rb = grab[handNumber].GetComponent<Rigidbody>();
            if (rb)
            {
                rb.isKinematic = false;
                // Give the object the same velocity as our hand.
                rb.velocity = OVRInput.GetLocalControllerVelocity(controller) * 1.5f + velocity;
            }
            grab[handNumber] = null;
        }
    }
}
