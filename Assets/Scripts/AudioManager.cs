﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioManager : MonoBehaviour
{
	public AudioClip StarWarpAudio;
	public AudioClip StarWarpCompleteAudio;
	public AudioClip StarGrabAudio;

	public AudioClip FinaleAudio;

	public AudioClip StateChangeAudio;

	public float SFXVolume;
	public float FanfareVolume;

	private Vector3 headsetRigPosition;


	private void OnEnable()
	{
		GalaxyEvents.OnWarpStart += OnWarpToStar;
		GalaxyEvents.OnGrabStar += OnGrabStar;
		//GalaxyEvents.OnWarpComplete += OnWarpComplete;
		//GalaxyEvents.OnStateChanged += OnStateChanged;
		GalaxyEvents.OnFinale += OnFinale;
		GalaxyEvents.OnUpdateHeadsetRigPosition += OnUpdateHeadsetRigPosition;
	}

	private void OnDisable()
	{
		GalaxyEvents.OnWarpStart -= OnWarpToStar;
		GalaxyEvents.OnGrabStar -= OnGrabStar;
		//GalaxyEvents.OnWarpComplete -= OnWarpComplete;
		//GalaxyEvents.OnStateChanged -= OnStateChanged;
		GalaxyEvents.OnFinale -= OnFinale;
		GalaxyEvents.OnUpdateHeadsetRigPosition -= OnUpdateHeadsetRigPosition;
	}


	private void OnWarpToStar(Vector3 startPosition, Star star)
	{
		if (StarWarpAudio != null)
			AudioSource.PlayClipAtPoint(StarWarpAudio, startPosition);
	}


	//private void OnWarpComplete(Star star)
	//{
	//	if (StarWarpCompleteAudio != null)
	//		AudioSource.PlayClipAtPoint(StarWarpCompleteAudio, headsetRigPosition);
	//}


	private void OnGrabStar(Star star)
	{
		if (StarGrabAudio != null)
			AudioSource.PlayClipAtPoint(StarGrabAudio, headsetRigPosition, SFXVolume);
	}

	private void OnFinale(Star finalStar)
	{
		if (FinaleAudio != null)
			AudioSource.PlayClipAtPoint(FinaleAudio, headsetRigPosition, FanfareVolume);
	}


	//private void OnStateChanged(ProgressionManager.ProgressionState newState)
	//{
		//{
		//	if (StateChangeAudio != null)
		//		AudioSource.PlayClipAtPoint(StateChangeAudio, headsetRigPosition, SFXVolume);
		//}
	//}

	private void OnUpdateHeadsetRigPosition(Vector3 position)
	{
		headsetRigPosition = position;
	}
}
