﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Galaxy : MonoBehaviour
{
    public static int MaxGeneration = 3;        // to limit auto-recursive activation

    public GameObject CentralStarPrefab;	
    public List<GameObject> StarPrefabs;	
    public List<Color> StarColours;

    public float SpawnDelay = 0.25f;

    private Star centralStar;           // seed (spawns cluster at start)
	private Vector3 randomRotation;		// passed down through generations

	public Color RandomStarColour { get { return StarColours[Random.Range(0, StarColours.Count)]; }}
    public GameObject RandomStarPrefab { get { return StarPrefabs[Random.Range(0, StarPrefabs.Count)]; }}


    private void Start()
    {
        LeanTween.init(2000);
        StartCoroutine(SpawnCentralStar());
    }

    private IEnumerator SpawnCentralStar()
    {
        yield return new WaitForSeconds(SpawnDelay);

        if (StarPrefabs.Count > 0)
        {
            var centralStarObj = Instantiate(CentralStarPrefab, transform);
            randomRotation = Random.rotation.eulerAngles;

            centralStar = centralStarObj.GetComponent<Star>();
            centralStar.SetCentralStar(true);
            centralStar.SetColour(Color.white);
            centralStar.SetRotation(randomRotation);
            centralStar.HeatStar(false, false, false);
            centralStar.HilightStar(true);
            centralStar.HideStar(true);
			centralStar.SetClusterStarPrefab(RandomStarPrefab);

            GalaxyEvents.OnStarSpawned?.Invoke(centralStar);

            centralStar.SpawnCluster(0, StarColours);       // central star is generation 0
        }
        yield return null;
    }
}
