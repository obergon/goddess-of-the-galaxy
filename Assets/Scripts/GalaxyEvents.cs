﻿using System;
using UnityEngine;

public static class GalaxyEvents
{
	public delegate void OnUpdateHeadsetRigPositionDelegate(Vector3 position);
	public static OnUpdateHeadsetRigPositionDelegate OnUpdateHeadsetRigPosition;

	public delegate void OnClusterSpawnedDelegate(StarCluster cluster);
	public static OnClusterSpawnedDelegate OnClusterSpawned;

	public delegate void OnStarSpawnedDelegate(Star star);
	public static OnStarSpawnedDelegate OnStarSpawned;

	public delegate void OnStarActivatedDelegate(Star star);
	public static OnStarActivatedDelegate OnStarActivated;      // only after manually 'heating'

	public delegate void OnStarHiddenDelegate(Star star);
	public static OnStarHiddenDelegate OnStarHidden;

	public delegate void OnHilightStarDelegate(Star star, bool on);
	public static OnHilightStarDelegate OnHilightStar;

	public delegate void OnGrabStarDelegate(Star star);
	public static OnGrabStarDelegate OnGrabStar;

	public delegate void OnHeatStarDelegate(Star star);
	public static OnHeatStarDelegate OnHeatStar;

	public delegate void OnCoolStarDelegate(Star star);
	public static OnCoolStarDelegate OnCoolStar;

	public delegate void OnStarRippleActivatedDelegate(Star star, StarRipple activatedBy);
	public static OnStarRippleActivatedDelegate OnStarRippleActivated;

	public delegate void OnWarpToStarDelegate(Vector3 startPosition, Star star);
	public static OnWarpToStarDelegate OnWarpStart;

	public delegate void OnWarpCompleteDelegate(Star star);
	public static OnWarpCompleteDelegate OnWarpComplete;

	public delegate void OnStateChangedDelegate(ProgressionManager.ProgressionState newState);
    public static OnStateChangedDelegate OnStateChanged;

	public delegate void OnFinaleDelegate(Star finalStar);
	public static OnFinaleDelegate OnFinale;

	public delegate void OnFinaleFinishDelegate();
	public static OnFinaleFinishDelegate OnFinaleFinish;
}

