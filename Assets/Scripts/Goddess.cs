﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goddess : MonoBehaviour
{
	private Star atStar;        // star goddess is located at

	private bool isWarping = false;

	private void OnEnable()
	{
		GalaxyEvents.OnWarpStart += OnWarpStart;
		GalaxyEvents.OnWarpComplete += OnWarpComplete;
	}

	private void OnDisable()
	{
		GalaxyEvents.OnWarpStart -= OnWarpStart;
		GalaxyEvents.OnWarpComplete -= OnWarpComplete;
	}

	private void Update()
	{
		// stars may be moving (galaxy rotating), so player needs to follow atStar
		if (atStar != null && !isWarping)
			gameObject.transform.position = atStar.transform.position;
	}


	// hide stars that are close to player
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Star"))
		{
			var star = other.gameObject.GetComponent<Star>();
			if (!star.HasBeenActivated)
				star.HideStar(true);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag("Star"))
		{
			var star = other.gameObject.GetComponent<Star>();
			if (!star.HasBeenActivated)
				star.HideStar(false);
		}
	}


	private void OnWarpStart(Vector3 startPosition, Star star)
	{
		// show star player was at
		if (atStar != null && !atStar.HasBeenActivated)
			atStar.HideStar(false);

		isWarping = true;
	}

	private void OnWarpComplete(Star star)
	{
		// hide star player warped to
		atStar = star;

		if (!star.HasBeenActivated)
			atStar.HideStar(true);

		isWarping = false;
	}
}
