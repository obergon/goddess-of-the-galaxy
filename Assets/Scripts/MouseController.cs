﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public float reachDistance = 100;
    public LayerMask starHitMask;

    public Star heatingStar = null;         // heat one star at a time - the one hit by mouse raycast

	//   private ProgressionManager.ProgressionState gameState = ProgressionManager.ProgressionState.Warp;

	//private bool CanHeat { get { return gameState >= ProgressionManager.ProgressionState.HeatStars; } }
	//private bool ChainReaction { get { return gameState >= ProgressionManager.ProgressionState.ChainReaction; } }


	private void OnEnable()
	{
		GalaxyEvents.OnStarActivated += OnStarActivated;
		GalaxyEvents.OnStateChanged += OnStateChanged;
    }

	private void OnDisable()
	{
        GalaxyEvents.OnStarActivated -= OnStarActivated;
        GalaxyEvents.OnStateChanged -= OnStateChanged;
	}


	private void Update()
    {
        RaycastHit hit;

        if (Input.GetMouseButtonUp(0))    // stop heating star
        {
            StopHeatingStar();
        }
        else if (Input.GetMouseButton(0))    // mouse held down
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit, reachDistance, starHitMask))
            {
                if (hit.collider.gameObject.CompareTag("Star"))
                {
                    var hitStar = hit.collider.gameObject.GetComponent<Star>();

                    if (heatingStar == null)            // no star heating, so heat hitStar
                    {
                        HeatStar(hitStar);
                    }
                    else if (heatingStar != hitStar)	// hitStar not heatingStar
                    {
                        StopHeatingStar();
                        HeatStar(hitStar);			// starts heating tween
                    }
					// do nothing if still pointing to heatingStar (will keep heating)
				}
            }
            else
                StopHeatingStar();
        }
    }

    private void HeatStar(Star star)
    {
		//Debug.Log("HeatStar: " + star);
		heatingStar = star;
        heatingStar.HilightStar(true);

        heatingStar.HeatStar();
    }

    private void StopHeatingStar()
    {
        if (heatingStar != null)
        {
			//Debug.Log("CoolHeatingStar: " + heatingStar);
			heatingStar.HilightStar(false);
            heatingStar.StopHeating();		// stop heating
        }
        heatingStar = null;
    }

	private void OnStarActivated(Star star)
	{
        //Debug.Log("OnStarActivated: " + star);
        //CoolHeatingStar();
    }

	private void OnStateChanged(ProgressionManager.ProgressionState newState)
    {
        //gameState = newState;
    }
}
