﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProgressionManager : MonoBehaviour
{
    public float stateChangeTimeRemaining;
    public float spawnTime, chainSpawnTime, fastSpawnTime;

    public int interactionsSinceStateChange;
    public int spawnInteractions, chainSpawnInteractions, fastSpawnInteractions;

    public bool debugMode;
    public Text debugUIText;

    public int StarLimit;			// triggers 'Finale' when reached
    public int starCount = 0;       // public so can see in inspector

    public ProgressionState CurrentProgressionState { get; private set; }

    public bool CanHeat { get { return CurrentProgressionState >= ProgressionState.HeatStars && !AtFinale; } }
	//public bool CanWarp { get { return CurrentProgressionState >= ProgressionState.Warp; } }
	public bool CanChainReaction { get { return CurrentProgressionState >= ProgressionState.ChainReaction && !AtFinale; } }
    public bool CanRapidHeat { get { return CurrentProgressionState >= ProgressionState.FastHeat && !AtFinale; } }
    public bool AtFinale { get { return CurrentProgressionState == ProgressionState.Finale; } }

    public enum ProgressionState
    {
        HeatStars = 10,
        //Warp = 20,
        ChainReaction = 30,
        FastHeat = 40,
        Finale = 50
    }


    private void OnEnable()
    {
        //GalaxyEvents.OnWarpToStar += OnWarpToStar;
        //GalaxyEvents.OnClusterSpawned += OnClusterSpawned;
        GalaxyEvents.OnStarSpawned += OnStarSpawned;
        GalaxyEvents.OnStarActivated += OnStarActivated;
        GalaxyEvents.OnStarHidden += OnStarHidden;
    }

	private void OnDisable()
    {
        //GalaxyEvents.OnWarpToStar -= OnWarpToStar;
        //GalaxyEvents.OnClusterSpawned -= OnClusterSpawned;
        GalaxyEvents.OnStarSpawned -= OnStarSpawned;
        GalaxyEvents.OnStarActivated -= OnStarActivated;
        GalaxyEvents.OnStarHidden -= OnStarHidden;
    }

	private void Start()
    {
        CurrentProgressionState = ProgressionState.HeatStars;
        //GalaxyEvents.OnStateChanged?.Invoke(CurrentProgressionState);

        stateChangeTimeRemaining = spawnTime;
        interactionsSinceStateChange = spawnInteractions;
        
        //Used in debug mode
        if (debugMode)
        {
            debugUIText.text = CurrentProgressionState.ToString();
        }
    }

    private void Update()
    {
        stateChangeTimeRemaining -= Time.deltaTime;

        if (stateChangeTimeRemaining <= 0f)
        {
            ChangeToNextState();
        }

		if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
		{
			print("UnityDebug: Current Progresson State: " + CurrentProgressionState);
			print("UnityDebug: Stars: " + GameObject.FindGameObjectsWithTag("Star").Length);
		}
	}

    private void ChangeToNextState()
    {
        switch (CurrentProgressionState)
        {
            case ProgressionState.HeatStars:
            {
				CurrentProgressionState = ProgressionState.ChainReaction;

				// reset counter for next state
				interactionsSinceStateChange = chainSpawnInteractions;
				stateChangeTimeRemaining = chainSpawnTime;

				GalaxyEvents.OnStateChanged?.Invoke(CurrentProgressionState);
				break;
			}
            case ProgressionState.ChainReaction:
            {
                CurrentProgressionState = ProgressionState.FastHeat;

                // reset counter for next state
                interactionsSinceStateChange = fastSpawnInteractions;
                stateChangeTimeRemaining = fastSpawnTime;

                GalaxyEvents.OnStateChanged?.Invoke(CurrentProgressionState);
                break;
            }
            case ProgressionState.FastHeat:
            {
                //CurrentProgressionState = ProgressionState.Finale;

                //// reset counter for next state
                //interactionsSinceStateChange = endingInteractions;
                //stateChangeTimeRemaining = endingTime;

                //GalaxyEvents.OnStateChanged?.Invoke(CurrentProgressionState);
                break;
            }
            case ProgressionState.Finale:
            {
                break;
            }
        }

        //print("UnityDebug: State Changed: " + CurrentProgressionState.ToString());

        if (debugMode)
        {
            debugUIText.text = CurrentProgressionState.ToString();
        }
    }

    //private void OnWarpToStar(Vector3 startPosition, Star star)
    //{
		//if (CanWarp)
		//{
  //          interactionsSinceStateChange--;
  //      }

  //      if (interactionsSinceStateChange <= 0)
  //      {
  //          ChangeToNextState();
  //      }
    //}

    private void OnStarSpawned(Star star)
    {
        starCount++;

		if (starCount >= StarLimit)
        {
            CurrentProgressionState = ProgressionState.Finale;
            GalaxyEvents.OnStateChanged?.Invoke(CurrentProgressionState);
            GalaxyEvents.OnFinale?.Invoke(star);
        }
    }

    //private void OnClusterSpawned(StarCluster cluster)
    //{
    //    //TODO - Add some stuff so that chain reactions only count as one interaction. 

    //    if (CurrentProgressionState >= ProgressionState.HeatStars)
    //    {
    //        interactionsSinceStateChange--;
    //    }

    //    if (interactionsSinceStateChange <= 0)
    //    {
    //        ChangeToNextState();
    //    }
    //}

    // only if a star activated by hand (heated up)
    private void OnStarActivated(Star star)
    {
        if (CanHeat)
        {
            interactionsSinceStateChange--;
        }

        if (interactionsSinceStateChange <= 0)
        {
            ChangeToNextState();
        }

        //print("UnityDebug: On Star Activate: (" + interactionsSinceStateChange + ")");
    }


    private void OnStarHidden(Star star)
    {
        starCount--;
        Debug.Log("OnStarHidden: " + starCount);
    }
}
