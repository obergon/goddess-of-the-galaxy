﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class Star : MonoBehaviour
{
	public StarCluster Cluster;
	public StarRipple StarRipple;
	public GameObject StarHilight;              // when pointed to
	public ParticleSystem LaunchTrail;          // when launched
	public GameObject lod01;                    //Gameobject that is the lod for distant stars.

	public ParticleSystem DeactivatedParticles;
	public ParticleSystem ActivatedParticles;
	public ParticleSystem HeatParticles;

	public GameObject player;                   //Player game object for the star to check distance for LOD    

	public float MinTwinkleDelay;               // particles enabled/disabled
	public float MaxTwinkleDelay;               // particles enabled/disabled
	public float MinTwinkleTime;                // particles enabled/disabled
	public float MaxTwinkleTime;                // particles enabled/disabled

	public int ClusterQuantity;                 // number of stars to spawn in cluster

	public float MinLaunchVelocity;             // for random speed
	public float MaxLaunchVelocity;             // for random speed

	public float KinematicDelay;                // delay until rigidbody disabled after spawning / launching
	public float LODDelay;                      // delay until LODCheck after spawning / launching
	public float ColliderDelay;                 // delay until collider enabled after spawning / launching

	private GameObject clusterStarPrefab;       // set from Galaxy

	public int NumChainGenerationsDeep;         // number of generations deep to activate chain reaction
	public int NumChainReactionStars;           // number of stars in this star's cluster that are 'chain activated'

	public AudioSource HilightAudio;
	public AudioSource HeatAudio;
	public AudioSource CoolAudio;
	public AudioSource GrabAudio;
	public AudioSource ChainReactionAudio;
	public AudioSource AutoActivateAudio;
	public List<AudioSource> ActivateAudio;

	private AudioSource RandomActivateAudio { get { return ActivateAudio[Random.Range(0, ActivateAudio.Count)]; } }

	public float rotationSpeed;
	private bool isRotating = false;
	private float rotationFactor = 0.00001f;

	public bool InheritRotation = true;         // from galaxy central star
	public Vector3 GalaxyRotation { get; private set; }          // set if InheritRotation

	public bool IsCentralStar { get; private set; }
	private List<Color> galaxyStarColours;      // as per Galaxy

	public Color StarColour { get; private set; }
	public bool HasBeenActivated { get; private set; }      // cluster spawned

	public float HeatScale;
	public float MaxHeatTime;
	public float MinHeatTime;
	public float ShrinkTime;
	public float HeatReduceTime;                // time to reduce from max to min heat time

	public float HilightRange;					// increases with star scale
	public float HilightIntensity;				// should increase with star scale?

	public bool EnableLOD = false;
	private bool lodCheckRoutineRunning = false;    //This is set when the coroutine is running or  waiting to run.
	public float lod1Distance;                  //The distance away from the player that the LOD01 setting takes effect

	// always positive value
	private float CheckPlayerDistance { get { return (player == null) ? 0f : Mathf.Abs(Vector3.Distance(gameObject.transform.position, player.transform.position)); } }

	private LTDescr heatTweenId;                // so it can be cancelled when cooling

	public float HeatDelay;
	private float starHeatTime;                 // current heat time
	private float heatTimeRemaining = 0f;
	private bool heatTimeReduced = false;
	private float shrinkStarDelay = 0.1f;       // after activation

	public int starLodFrames;                   //Holds the number of frames over which the stars will check the LOD after events
	private int starLodNumber;                  //Holds an int that defines how many frames the star waits before checking the player distance.
	private bool lodSpriteShowing = false;

	public AudioClip HideStarAudio;				// TODO: remove!
	public AudioClip ShowStarAudio;				// TODO: remove!

	//TESTING
	//private bool hasHeated;

	private bool playerMoving;  //This is used to check if the player is moving so the star can check LODs
	private ProgressionManager progression;


	private enum HeatState
	{
		None,
		Heating,
		Cooling
	}
	private HeatState heatState = HeatState.None;

	private bool isHilighted = false;

	public int Generation;            // 0 = galactic central star

	public bool debugSpawn;         // Enable this to spawn using "s"
	public bool debugHeat;         // Enable this to spawn using "h" (heat) or "c" (cool)


	private void OnEnable()
	{
		GalaxyEvents.OnWarpStart += WarpStarted;
		GalaxyEvents.OnWarpComplete += WarpComplete;
		GalaxyEvents.OnStateChanged += OnStateChanged;
		GalaxyEvents.OnFinale += OnFinale;
	}

	private void OnDisable()
	{
		GalaxyEvents.OnWarpStart -= WarpStarted;
		GalaxyEvents.OnWarpComplete -= WarpComplete;
		GalaxyEvents.OnStateChanged -= OnStateChanged;
		GalaxyEvents.OnFinale -= OnFinale;
	}


	// set star states according to progression state at time of spawn
	private void Awake()
	{
		progression = GameObject.FindGameObjectWithTag("ProgressionManager").GetComponent<ProgressionManager>();

		if (progression.CanRapidHeat)
			ReduceHeatTime();

		HasBeenActivated = false;
		ActivatedParticles.Stop();
		ActivatedParticles.gameObject.SetActive(false);
		HeatParticles.Stop();
		DeactivatedParticles.Play();

		starHeatTime = MaxHeatTime;
		heatTimeRemaining = starHeatTime;

		//randomRotation = Random.rotation;

		//StartCoroutine(DisablePhysics());               // disable rigidbody after a delay (physics optimisation)
		StartCoroutine(DelayCollider());                // to prevent raycast hit on instantiation
		StartCoroutine(DelayLODCheck());                // check LOD after launch

		starLodNumber = progression.starCount % starLodFrames;  //Used to define the frame in which the star will ckeck the LOD after warp
	}

	//private void Start()
	//{
	//	StartCoroutine(LODCheck(starLodNumber));
	//}

	private void Update()
	{
		//if (InheritRotation && GalaxyRotation != Vector3.zero)
		//if (IsCentralStar && GalaxyRotation != Vector3.zero)
		//	transform.Rotate(GalaxyRotation * rotationSpeed * rotationMultiplier);

		if (isRotating && GalaxyRotation != Vector3.zero)
			transform.Rotate(GalaxyRotation * rotationSpeed * rotationFactor);

		//Test code to spawn cluster if the s key is pressed and the debug spawn boot set to true.
		if (Input.GetKeyDown(KeyCode.S))
		{
			if (debugSpawn)
				StartCoroutine(SpawnCluster(false, 1, 0));
		}

		if (debugHeat)
		{
			if (Input.GetKeyDown(KeyCode.H))
			{
				HeatStar();
			}

			if (Input.GetKeyDown(KeyCode.C))
			{
				StopHeating();
			}
		}

		// TODO: reinstate??  !!Ready to reinstate!!
		//if (!playerMoving && !lodCheckRoutineRunning)
		//{
		//	StartCoroutine(LODCheck(starLodNumber + 300));
		//}

		////Used for debuging
		//if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
		//{
		//	//print("UnityDebug: Star can chain spawn: " + ChainReaction);
		//}
	}

	private IEnumerator DisablePhysics()
	{
		var rBody = GetComponent<Rigidbody>();

		yield return new WaitForSeconds(KinematicDelay);
		rBody.isKinematic = true;
	}

	private IEnumerator DelayCollider()
	{
		var collider = GetComponent<Collider>();

		collider.enabled = false;
		yield return new WaitForSeconds(ColliderDelay);
		collider.enabled = true;
	}

	private IEnumerator DelayLODCheck()
	{
		yield return new WaitForSeconds(LODDelay);
		StartCoroutine(LODCheck(starLodNumber));
	}

	public void SpawnCluster(int generation, List<Color> starColours)
	{
		if (HasBeenActivated)
			return;

		Generation = generation;
		galaxyStarColours = starColours;

		StartCoroutine(SpawnCluster(false));
	}

	// generationsDeep & numChainReactionStars are used for chain reaction spawning
	public IEnumerator SpawnCluster(bool chainReaction, int generationsDeep = 1, int numChainReactionStars = 0, bool autoActivated = false)
	{
		//print("UnityDebug: Spawn Cluster Called");
		if (clusterStarPrefab == null)
			yield break;

		if (HasBeenActivated)
			yield break;

		if (progression.AtFinale)
			yield break;        // no more stars

		if (Cluster != null)
		{
			HasBeenActivated = true;

			if (chainReaction)
				ChainReactionAudio.Play();
			else if (autoActivated)
				AutoActivateAudio.Play();
			else
				RandomActivateAudio.Play();

			if (!chainReaction && !autoActivated)
			{
				GalaxyEvents.OnStarActivated?.Invoke(this);		// only if manually heated / activated
			}

			ShrinkStar(true);     // shrink + deactivate

			var spawnQuantity = (chainReaction && numChainReactionStars > 0) ? numChainReactionStars : ClusterQuantity;
			yield return StartCoroutine(Cluster.SpawnStars(clusterStarPrefab, spawnQuantity, StarColour, galaxyStarColours, Generation + 1));

			if (chainReaction && !autoActivated && generationsDeep > 1)           // not yet end of chain reaction
			{
				yield return StartCoroutine(Cluster.ActivateChainReaction(--generationsDeep, numChainReactionStars));    // recursive
			}
		}

		//ExpandRipple();
		yield return null;
	}


	// expand star
	public void HeatStar(bool activateWhenHot = true, bool autoActivated = false, bool particles = true)
	{
		if (HasBeenActivated)
			return;

		if (!progression.CanHeat && !autoActivated)
			return;

		//print("UnityDebug: HeatStar(). Can Chain = " + progression.CanChainReaction);

		if (heatState != HeatState.Heating)         // first time
			heatTimeRemaining = starHeatTime;

		heatState = HeatState.Heating;

		if (particles)
			HeatParticles.Play();

		var hilight = StarHilight.GetComponent<Light>();

		heatTweenId = LeanTween.scale(DeactivatedParticles.gameObject, new Vector3(HeatScale, HeatScale, HeatScale), heatTimeRemaining)
					.setEase(LeanTweenType.linear)
					.setDelay(HeatDelay)
					.setOnStart(() =>
					{
						GalaxyEvents.OnHeatStar?.Invoke(this);
						HeatAudio.Play();
					})
					.setOnUpdate((Vector3 val) =>
					{
						HeatParticles.transform.localScale = val;
						hilight.range = HilightRange * val.x;
						//hilight.intensity = HilightIntensity * val.x;
						heatTimeRemaining -= Time.deltaTime;        // so we can resume interrupted tween
					})
					.setOnComplete(() =>
					{
						if (activateWhenHot)
						{
							int generationsDeep = (progression.CanChainReaction && !autoActivated) ? NumChainGenerationsDeep : 1;
							int numChainReactionStars = (progression.CanChainReaction && !autoActivated) ? NumChainReactionStars : 0;

							StartCoroutine(SpawnCluster(progression.CanChainReaction && !autoActivated, generationsDeep, numChainReactionStars, autoActivated));
							//print("UnityDebug: SpawnCluster called from HeatStar(). Can Chain = " + progression.CanChainReaction);

							//ShrinkStar(true);     // shrink + deactivate
						}

						heatTimeRemaining = 0f;
					});
	}

	private void ShrinkStar(bool deactivate)
	{
		HeatParticles.Stop();
		var hilight = StarHilight.GetComponent<Light>();

		LeanTween.scale(DeactivatedParticles.gameObject, Vector3.one, ShrinkTime)
			.setEase(LeanTweenType.linear)
			.setDelay(shrinkStarDelay)
			.setOnUpdate((Vector3 val) =>
			{
				hilight.range = HilightRange * val.x;
				//hilight.intensity = HilightIntensity * val.x;
				//HeatParticles.transform.localScale = val;
			})
			.setOnComplete(() =>
			{
				if (deactivate)
					DeactivateStar();
				//CoolAudio.Play();
			});
	}

	public void StopHeating()
	{
		if (HasBeenActivated)
			return;

		if (heatState != HeatState.Heating)
			return;

		HeatParticles.Stop();

		// stop the heating tween
		if (heatTweenId != null)
		{
			LeanTween.cancel(heatTweenId.id);
			heatTweenId = null;
		}
	}

	public void ReduceHeatTime()
	{
		if (heatTimeReduced)
			return;

		heatTimeReduced = true;

		LeanTween.value(gameObject, MaxHeatTime, MinHeatTime, HeatReduceTime)
							.setEase(LeanTweenType.linear)
							.setOnUpdate((float val) =>
							{
								starHeatTime = val;
							});
	}

	// launch in random direction and random velocity
	public void LaunchStar()
	{
		if (HasBeenActivated)
			return;

		ActivatedParticles.Stop();
		DeactivatedParticles.Play();

		StartCoroutine(TwinkleStar());

		float spawnSpeed = Random.Range(MinLaunchVelocity, MaxLaunchVelocity);

		// offset sphere centre to centre of this star
		var launchDirection = (Random.onUnitSphere + transform.position) - transform.position;

		// launch
		//GetComponent<Rigidbody>().velocity = new Vector3(spawnX, spawnY, spawnZ).normalized * spawnSpeed;
		GetComponent<Rigidbody>().velocity = launchDirection * spawnSpeed;

		if (LaunchTrail != null)
			LaunchTrail.Play();
	}

	public void SetClusterStarPrefab(GameObject starPrefab)
	{
		clusterStarPrefab = starPrefab;
	}

	public void SetColour(Color colour)
	{
		StarColour = colour;

		var deactivatedMain = DeactivatedParticles.main;
		var activatedMain = ActivatedParticles.main;

		deactivatedMain.startColor = colour;
		activatedMain.startColor = colour;

		lod01.GetComponent<SpriteRenderer>().color = colour;
	}

	public void SetRotation(Vector3 rotation)
	{
		if (InheritRotation)
			GalaxyRotation = rotation;
	}

	// randomise particle enable/disable
	private IEnumerator TwinkleStar()
	{
		while (!HasBeenActivated)
		{
			yield return new WaitForSeconds(Random.Range(MinTwinkleDelay, MaxTwinkleDelay));
			DeactivatedParticles.Stop();
			yield return new WaitForSeconds(Random.Range(MinTwinkleTime, MaxTwinkleTime));
			DeactivatedParticles.Play();
			yield return null;
		}

		yield return null;
	}

	private void DeactivateStar()
	{
		if (IsCentralStar)
			return;

		if (!HasBeenActivated)
			return;

		GetComponent<Renderer>().enabled = false;
		GetComponent<Collider>().enabled = false;
		GetComponent<Rigidbody>().isKinematic = true;       // disabled

		ActivatedParticles.Stop();
		DeactivatedParticles.Stop();
		HeatParticles.Stop();

		HilightStar(false);

		ActivatedParticles.gameObject.SetActive(false);
		DeactivatedParticles.gameObject.SetActive(false);
		ShowLodSprite(false);
		//HeatParticles.gameObject.SetActive(false);
		StarHilight.gameObject.SetActive(false);

		GalaxyEvents.OnStarHidden?.Invoke(this);
	}

	public void HideStar(bool hide)
	{
		if (hide)
		{
			DeactivatedParticles.gameObject.SetActive(false);
			DeactivatedParticles.Stop();
			HeatParticles.Stop();
			lod01.SetActive(false);

			//if (HideStarAudio != null)
			//	AudioSource.PlayClipAtPoint(HideStarAudio, transform.position);
		}
		else			// show star!
		{
			if (lodSpriteShowing)
				lod01.SetActive(true);
			else
			{
				DeactivatedParticles.gameObject.SetActive(true);
				DeactivatedParticles.Play();
			}

			//if (ShowStarAudio != null)
			//	AudioSource.PlayClipAtPoint(ShowStarAudio, transform.position);
		}
	}

	public void SetGeneration(int generation)
	{
		Generation = generation;
	}

	// true if in same cluster
	public bool IsSiblingTo(Star star)
	{
		return Cluster.gameObject.GetInstanceID() == star.Cluster.gameObject.GetInstanceID();
	}

	public void SetCentralStar(bool isCentre)
	{
		IsCentralStar = isCentre;
	}

	//private void ExpandRipple()
	//{
	//	if (StarRipple != null)
	//	{
	//		//StarRipple.gameObject.SetActive(true);
	//		StarRipple.Expand();
	//	}
	//}

	public void HilightStar(bool on)
	{
		if (HasBeenActivated)
			return;

		StarHilight.gameObject.SetActive(on);
		isHilighted = on;

		GalaxyEvents.OnHilightStar?.Invoke(this, on);

		if (isHilighted)
			HilightAudio.Play();
	}

	private void WarpStarted(Vector3 startPosition, Star star)
	{
		playerMoving = true;

		if (EnableLOD)
			StartCoroutine(LODCheck(starLodNumber));
	}

	private void WarpComplete(Star star)
	{
		playerMoving = false;

		// reshow central star (after all warps)
		if (IsCentralStar)
			HideStar(false);

		//if (EnableLOD)
		//	StartCoroutine(LODCheck(starLodNumber));
	}


	private void OnStateChanged(ProgressionManager.ProgressionState newState)
	{
		//if (progressionState == newState)
		//	return;

		//progressionState = newState;

		switch (newState)
		{
			//case ProgressionManager.ProgressionState.Warp:
			case ProgressionManager.ProgressionState.HeatStars:
			case ProgressionManager.ProgressionState.ChainReaction:
			case ProgressionManager.ProgressionState.Finale:
				break;

			case ProgressionManager.ProgressionState.FastHeat:
				ReduceHeatTime();       // gradually reduces heat time
				break;

			default:
				break;
		}
	}


	private void OnFinale(Star finalStar)
	{
		ShrinkStar(false);

		if (IsCentralStar)
			isRotating = true;
	}


	private IEnumerator LODCheck(int waitFrames)
	{
		//Debug.Log("LODCheck: waitFrames = " + waitFrames);
		lodCheckRoutineRunning = true;

		if (!EnableLOD)
		{
			lodCheckRoutineRunning = false;
			yield break;
		}

		if (HasBeenActivated)
		{
			lodCheckRoutineRunning = false;
			yield break;
		}

		for (int i = 0; i < waitFrames; i++)
		{
			yield return null;
		}

		if (player == null)
		{
			player = GameObject.FindGameObjectWithTag("MainCamera");

			if (player == null)
				yield break;
		}

		if (CheckPlayerDistance > lod1Distance)         // out of range - enable sprite
		{
			//if (lod01.activeInHierarchy == false)
			{
				DeactivatedParticles.Stop();
				DeactivatedParticles.gameObject.SetActive(false);
				ShowLodSprite(true);
			}
		}
		else                                            // in range - enable particles
		{
			//if (DeactivatedParticles.gameObject.activeInHierarchy == false)
			{
				ShowLodSprite(false);
				DeactivatedParticles.gameObject.SetActive(true);
				DeactivatedParticles.Play();
			}
		}

		lodCheckRoutineRunning = false;

		//Debug.Log("CheckPlayerDistance: " + CheckPlayerDistance() + ", lod1Distance: " + lod1Distance);

		if (playerMoving)
		{
			StartCoroutine(LODCheck(starLodFrames));
		}
	}

	private void ShowLodSprite(bool show)
	{
		lod01.SetActive(show);
		lodSpriteShowing = show;
	}
}
	