﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class StarCluster : MonoBehaviour
{
	public bool UseParentColour = true;			// if true, cluster stars inherit colour from parent (except first generation)

	public bool AutoActivateStars = true;       // if true, cluster stars are activated after random delays.  Limited generations deep.
	public float MinAutoActivateDelay;			// for auto star activation 
	public float MaxAutoActivateDelay;          // for auto star activation

	public float StarLaunchDelay = 0.1f;		// pause between launch of each star in cluster
	public float ChainReactionDelay = 0.5f;		// pause between chain reaction activations

	public int Generation;                      // 0 = galactic central star. set in code (for visibility only)

	private List<Star> starList = new List<Star>();         // stars in this cluster (also child objects)
	private GameObject clusterStarPrefab;					// prefab for each star in this cluster
	private GameObject nextGenStarPrefab;					// prefab for the cluster of each star in this cluster

	private List<Color> galaxyStarColours;
	private Color parentStarColour;

	private Galaxy galaxy;


	private Color RandomStarColour { get { return galaxyStarColours[Random.Range(0, galaxyStarColours.Count)]; }}


	private void Awake()
	{
		var galaxyObj = GameObject.FindWithTag("Galaxy");
		galaxy = galaxyObj.GetComponent<Galaxy>();
	}


	public IEnumerator SpawnStars(GameObject starPrefab, int spawnQuantity, Color parentColour, List<Color> starColours, int generation)
	{
		clusterStarPrefab = starPrefab;
		nextGenStarPrefab = galaxy.RandomStarPrefab;       // randomise next gen star prefab (each star's cluster)

		Generation = generation;

		parentStarColour = parentColour;
		galaxyStarColours = starColours;

		for (int i = 0; i < spawnQuantity; i++)
		{
			var newStarObj = Instantiate(clusterStarPrefab, transform.position, Quaternion.identity, transform);
			Star parentStar = transform.parent.GetComponent<Star>();
			Star newStar = newStarObj.GetComponent<Star>();

			// first generation stars are random colours
			newStar.SetColour(UseParentColour && generation > 1 ? parentStarColour : RandomStarColour);     
			newStar.SetClusterStarPrefab(nextGenStarPrefab);
			newStar.SetGeneration(generation);
			newStar.SetRotation(parentStar.GalaxyRotation);

			starList.Add(newStar);

			GalaxyEvents.OnStarSpawned?.Invoke(newStar);
			yield return null;
		}

		yield return StartCoroutine(LaunchStars()); 
		GalaxyEvents.OnClusterSpawned?.Invoke(this);

		// recursive activation (self-activate after random 'lifetime')
		if (AutoActivateStars && Generation < Galaxy.MaxGeneration)
			StartCoroutine(AutoActivateClusterStars());
	}


	// launch stars in this cluster
	private IEnumerator LaunchStars()
	{
		foreach (var star in starList)
		{
			//yield return new WaitForSeconds(StarLaunchDelay);
			star.LaunchStar();
		}

		yield return null;
	}


	// recursive activation of cluster stars
	public IEnumerator ActivateChainReaction(int generationsDeep, int numClusterStars)      // 0 numClusterStars == all stars in cluster
	{
		int starCount = 0;
		//Debug.Log("ActivateChainReaction: generationsDeep = " + generationsDeep + ", numClusterStars = " + numClusterStars);

		foreach (var star in starList)
		{
			if (star.HasBeenActivated)
				continue;

			yield return new WaitForSeconds(ChainReactionDelay);
			yield return StartCoroutine(star.SpawnCluster(true, generationsDeep, numClusterStars, false));
			starCount++;

			if (numClusterStars > 0 && starCount == numClusterStars)
				break;
		}

		yield return null;
	}

	// recursive activation after random 'lifetime'
	private IEnumerator AutoActivateClusterStars()
	{
		foreach (var star in starList)
		{
			yield return new WaitForSeconds(Random.Range(MinAutoActivateDelay, MaxAutoActivateDelay));
			star.HeatStar(true, true);            // spawns cluster when fully 'heated'.  No chain reaction
		}

		yield return null;
	}
}
