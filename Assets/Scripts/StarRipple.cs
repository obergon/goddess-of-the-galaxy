﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StarRipple : MonoBehaviour
{
    public Star ParentStar;
    public float ExpandTime;
    public float ExpandScale;

    public Color RippleColour;

    public bool ActivateStars;			// trigger collider activates stars of same colour (not siblings)


    private void Start()
    {
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
    }

	private void OnTriggerEnter(Collider other)
	{
        if (! ActivateStars)
            return;

        if (! other.gameObject.CompareTag("Star"))
            return;

        if (other.gameObject.GetInstanceID() == ParentStar.gameObject.GetInstanceID())
            return;

        Star collisionStar = other.GetComponent<Star>();
        if (collisionStar.HasBeenActivated)
            return;

		// don't trigger siblings (same cluster)
        if (collisionStar.IsSiblingTo(ParentStar))
            return;

        // only trigger stars of same colour as this star
        if (collisionStar.StarColour != ParentStar.StarColour)
            return;

        //Debug.Log("Ripple triggered star");

        // activate star (spawn its cluster)
        //collisionStar.SpawnCluster();			// TODO: fix recursion?

        GalaxyEvents.OnStarRippleActivated?.Invoke(collisionStar, this);
    }


    public void Expand()
    {
        var renderer = GetComponent<MeshRenderer>();
        var collider = GetComponent<Collider>();

        renderer.enabled = true;
        collider.enabled = true;
        renderer.material.color = RippleColour;

        // disable star collider while ripple is expanding
        var parentCollider = ParentStar.GetComponent<Collider>();
        parentCollider.enabled = false;

        LeanTween.scale(gameObject, new Vector3(ExpandScale, ExpandScale, ExpandScale), ExpandTime)
							.setEase(LeanTweenType.easeOutCirc)
                            .setOnComplete(() =>
                            {
                                transform.localScale = Vector3.one;
                            });

        LeanTween.value(gameObject, RippleColour, Color.clear, ExpandTime)
							.setEase(LeanTweenType.easeOutCubic)
                            .setOnUpdate((Color val) =>
                            {
                                renderer.material.color = val;
                            })
							.setOnComplete(() =>
                            {
                                renderer.enabled = false;
                                collider.enabled = false;

                                renderer.material.color = RippleColour;		// reset

                                parentCollider.enabled = true;        // needed for raycast detection
                            });
    }
}
