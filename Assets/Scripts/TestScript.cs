﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public int starsToSpawn;
    public GameObject star;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < starsToSpawn; i++)
        {
            Instantiate(star, new Vector3(Random.Range(-50,50), Random.Range(-50, 50), Random.Range(-50, 50)), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
