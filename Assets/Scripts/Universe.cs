﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Universe : MonoBehaviour
{
	public List<GameObject> GalaxyPrefabs = new List<GameObject>();
	public float UniverseRadius;
	public int NumGalaxies;
	public float MinScale;
	public float MaxScale;
	public float MinTransparency;		// percent
	public float MaxTransparency;		// percent

	private GameObject RandomGalaxy { get { return GalaxyPrefabs[ Random.Range(0, GalaxyPrefabs.Count) ]; } }
	private float RandomTransparency { get { return Random.Range(MinTransparency, MaxTransparency); } }


	public void Start()
	{
		SpawnGalaxies();
	}

	private void SpawnGalaxies()
	{
		for (int i = 0; i < NumGalaxies; i++)
		{
			var galaxy = Instantiate(RandomGalaxy, Random.onUnitSphere * UniverseRadius, Quaternion.identity, transform);
			galaxy.transform.rotation = Random.rotation;

			galaxy.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, RandomTransparency / 100f);

			var xScale = Random.Range(MinScale, MaxScale);
			var yScale = Random.Range(MinScale, MaxScale);
			galaxy.transform.localScale = new Vector3(xScale, yScale, 1);
		}
	}
}
