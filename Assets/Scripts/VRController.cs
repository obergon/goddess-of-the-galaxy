﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class VRController : MonoBehaviour
{
    public LineRenderer controllerLineRenderer;
    public GameObject controllerModel;
    public GameObject centreEyeTracker;
    public GameObject headsetRig;
    public float warpSpeed;

    public float FinaleWarpDistance = 200;			// from final star spawned
    public float FinaleWarpSpeed = 2.0f;

    public float maxGrabDistance = 100;
    public LayerMask grabMask;

    private float grabTimer = 0;  //Timer for holding the trigger button
    public float grabTime;
    private float grabDistance;

    private bool triggerDown = false;
    private bool isGrabbing = false;

    private GameObject grabbedObject;
    private GameObject hoverObject;
    private Star heatingStar;

	//TESTING LINE RENDERER
	//public LineRenderer testLineRenderer;


	// Start is called before the first frame update
	void Start()
    {
        GalaxyEvents.OnUpdateHeadsetRigPosition?.Invoke(headsetRig.transform.position);
        //controllerLineRenderer.material.color = Color.magenta;
    }

    private void OnEnable()
    {
        //GalaxyEvents.OnFinale += FinaleWarp;
    }

    private void OnDisable()
    {
        //GalaxyEvents.OnFinale -= FinaleWarp;
    }


    // Update is called once per frame
    void Update()
    {
        controllerLineRenderer.SetPositions(new Vector3[] { controllerModel.transform.position, (controllerModel.transform.forward * 5) + controllerModel.transform.position });
        RaycastHit hit;

        if (Physics.Raycast(centreEyeTracker.transform.position, (controllerLineRenderer.GetPosition(1) - centreEyeTracker.transform.position).normalized, out hit, maxGrabDistance, grabMask))
        {
            if (hit.collider.gameObject.CompareTag("Star"))
            {
                var hitStar = hit.collider.gameObject.GetComponent<Star>();

                if (heatingStar == null)                    // no star heating, so heat hitStar
                {
                    HeatStar(hitStar);
                }
                else if (heatingStar != hitStar)        // hitStar not heatingStar
                {
                    CoolHeatingStar();
                    HeatStar(hitStar);          // starts heating tween
                }

                if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
                {
                    triggerDown = true;
                    grabDistance = Vector3.Distance(controllerModel.transform.position, hit.collider.transform.position);
                    //controllerLineRenderer.material.color = Color.green;
                    grabbedObject = hit.collider.gameObject;
                    CoolHeatingStar();
                }
            }
            else
            {
                CoolHeatingStar();
            }
        }
        else
        {
            CoolHeatingStar();
        }

        // warp on trigger up
        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
        {
            //controllerLineRenderer.material.color = Color.magenta;
            triggerDown = false;
            grabTimer = 0;

            if (!isGrabbing && grabbedObject != null)
            {
                WarpToStar(grabbedObject.GetComponent<Star>());
            }
            else if (isGrabbing && grabbedObject != null)
            {
                var rBody = grabbedObject.GetComponent<Rigidbody>();
                rBody.isKinematic = false;
                rBody.velocity = (headsetRig.transform.position - grabbedObject.transform.position).normalized * 50f;
            }

            isGrabbing = false;
            grabbedObject = null;
        }

        if (triggerDown && !isGrabbing)
        {
            grabTimer += Time.deltaTime;
            if (grabTimer >= grabTime)
            {
                isGrabbing = true;
                if (grabbedObject != null)
                {
                    var star = GetComponent<Star>();
                    //star.GrabAudio.Play();
                    GalaxyEvents.OnGrabStar?.Invoke(star);
                }
            }
        }

        if (isGrabbing)
        {
            if (grabbedObject != null)
            {
                grabbedObject.transform.position = controllerModel.transform.position + (controllerModel.transform.forward.normalized * grabDistance);
            }
        }
    }

	// moves headset rig to star position
    private void WarpToStar(Star toStar)
    {
        //controllerLineRenderer.material.color = Color.green;
        GalaxyEvents.OnWarpStart?.Invoke(headsetRig.transform.position, toStar);
        controllerLineRenderer.enabled = false;

        LeanTween.move(headsetRig, toStar.transform.position, warpSpeed)
                   .setEase(LeanTweenType.easeInQuart)
                   .setOnComplete(() =>
                   {
                       GalaxyEvents.OnUpdateHeadsetRigPosition?.Invoke(headsetRig.transform.position);
                       GalaxyEvents.OnWarpComplete?.Invoke(toStar);
                       controllerLineRenderer.enabled = true;
                   });
    }


	private Vector3 Intersect(Ray ray, Star finalStar)
    {
        Vector3 L = Vector3.zero - (ray.start);

        float tca = Vector3.Dot(L, ray.delta);
        if (tca < 0) return Vector3.zero;
        float d2 = Vector3.Dot(L, L) - tca * tca;
        if (d2 > FinaleWarpDistance * FinaleWarpDistance) return Vector3.zero;
        float thc = (float)Math.Sqrt(FinaleWarpDistance * FinaleWarpDistance - d2);
        float t0 = tca - thc;
        float t1 = tca + thc;
        float m = Math.Min(t0, t1);
        return ray.interpolate(m);
    }

    private void FinaleWarp(Star finalStar)
    {
		var directionFromStar = headsetRig.transform.position - finalStar.transform.position;
		var distanceFromStar = directionFromStar.magnitude;
		var dirFromStarNorm = directionFromStar.normalized;
		Ray ray = new Ray(headsetRig.transform.position + -headsetRig.transform.forward * FinaleWarpDistance * 10,
			headsetRig.transform.forward);

		var distanceToWarp = FinaleWarpDistance - distanceFromStar;
		var warpPosition = Intersect(ray, finalStar);

		LeanTween.move(headsetRig, warpPosition, FinaleWarpSpeed)
							.setEase(LeanTweenType.easeInQuart)
							.setOnComplete(() =>
							{
								GalaxyEvents.OnUpdateHeadsetRigPosition?.Invoke(headsetRig.transform.position);
								GalaxyEvents.OnFinaleFinish?.Invoke();
							});
	}

    private void HeatStar(Star star)
    {
        if (triggerDown)
            return;

        //Debug.Log("HeatStar: " + star);
        heatingStar = star;
        heatingStar.HilightStar(true);
        //isHeating = true;

        heatingStar.HeatStar();
    }

    private void CoolHeatingStar()
    {
        if (heatingStar != null)
        {
            //print("UnityDebug: CoolHeatingStar: " + heatingStar);
            heatingStar.HilightStar(false);
            //isHeating = false;
            heatingStar.StopHeating();		// stop heating
        }
        heatingStar = null;
    }


	// class used by (now redundant) finale warp
    class Ray
    {
        public Vector3 start;
        public Vector3 delta;

        public Ray()
        {

        }

        public Ray(Vector3 s, Vector3 d)
        {
            start = s;
            delta = d;
        }

        public Vector3 interpolate(float value)
        {
            return start + delta * value;
        }
    }
}
